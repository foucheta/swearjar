from django.apps import AppConfig


class SwearJarConfig(AppConfig):
    name = 'swear_jar'
