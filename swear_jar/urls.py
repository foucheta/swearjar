from django.conf.urls import url

from . import views

app_name = 'swear_jar'

urlpatterns = [
    url(r'^test/$', views.test, name='test'),
    url(r'^$', views.welcome, name='homepage'),
    url(r'^join/$', views.join_swear_jar, name='join_swear_jar'),
    url(r'^create_organisation/$', views.create_organisation_form, name='create_organisation'),
    url(r'^created_organisation/$', views.created_organisation, name='created_organisation'),
    url(r'^search_organisation/$', views.search_organisation, name='search_organisation'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^log/$', views.log, name='logs'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^results/$', views.organisation_results, name='results'),
    url(r'^(?P<organisation_id>[0-9]+)/create_player/$', views.create_player_form, name='create_player'),
    url(r'^create_player/$', views.create_player_form, name='create_player_orgaless'),
    url(r'^(?P<organisation_id>[0-9]+)/created_player/$', views.created_player, name='created_player'),
    url(r'^created_player/$', views.created_player, name='created_player_orgaless'),
    url(r'^organisation_admin/$', views.organisation_admin, name='organisation_admin'),
    url(r'^change_player/$', views.change_player_status, name='change_player'),
    url(r'^denounce/$', views.denounce, name='denounce_curse'),
    url(r'^denounced/$', views.denounced, name='denounced_curse'),
    url(r'^regroup_curse/$', views.regroup_curse, name='regroup_curse'),
    url(r'^reported_curse/$', views.reported_curse, name='reported_curse'),
    url(r'^player_graph/$', views.player_graph, name='player_graph'),
    url(r'^player_home/$', views.player_home, name='player_home'),
    url(r'^player_edit/$', views.player_edit, name='player_edit'),
    url(r'^player_edited/$', views.player_edited, name='player_edited'),
    url(r'^player_change_password/$', views.player_change_password, name='player_change_password'),
    url(r'^player_bank/$', views.player_bank, name='player_bank'),
    url(r'^player_bank_paid/$', views.player_paid_bank, name='player_bank_paid'),
    url(r'^api/search_organisation$', views.api_search, name='api_search_orga'),
    url(r'^api/join_organisation/(?P<organisation_id>[0-9]+)$', views.api_join_organisation,
        name='api_join_organisation'),
    url(r'^api/accept_curse$', views.api_accept_curse, name='api_accept_curse'),
    url(r'^api/reject_curse$', views.api_reject_curse, name='api_reject_curse'),
]
