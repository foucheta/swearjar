from django.test import TestCase, Client
from django.test.utils import setup_test_environment

from django.core.urlresolvers import reverse

setup_test_environment()

# Create your tests here.

class IndexViewTests(TestCase):
    def test_view_accessible(self):
        client = Client()
        reponse = client.get(reverse('swear_jar:index'))
        self.assertEqual(reponse.status_code, 200)
