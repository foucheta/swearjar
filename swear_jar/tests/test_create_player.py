from django.test.utils import setup_test_environment
from django_webtest import WebTest

from django.core.urlresolvers import reverse

from ..models import Player, Organisation

setup_test_environment()

# Create your tests here.

class CreatePlayerTests(WebTest):
    def test_create_player_view(self):
        orga = Organisation(name='Dolead')
        orga.save()
        # form = self.app.get(reverse('swear_jar:create_player')).form
        form = self.app.get(reverse('swear_jar:create_player',
                                    kwargs={'organisation_id': orga.id})).form
        # ask for a player name & email
        self.assertTrue('player_name' in form.fields)
        self.assertTrue('player_password' in form.fields)
        self.assertTrue('player_email' in form.fields)

        form['player_name'] = 'Arnaud'
        form['player_email'] = 'arnaud@yopmail.com'
        form['player_password'] = 'aaaaaaaa'
        form.submit()

        player = Player.objects.first()
        self.assertEqual(player.name, 'Arnaud')
        self.assertEqual(player.email, 'arnaud@yopmail.com')
        self.assertEqual(player.organisation.name, 'Dolead')
