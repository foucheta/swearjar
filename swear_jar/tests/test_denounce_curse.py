from django.test.utils import setup_test_environment
from django_webtest import WebTest

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from datetime import date

from ..models import Curse, Player, Organisation

setup_test_environment()

# Create your tests here.

class DenounceCurseTests(WebTest):
    def test_denounce_curse_view(self):
        orga = Organisation(name='Dolead')
        orga.save()
        user = User(username='Francois', password='aaaaaaaa')
        user.save()
        player = Player(user=user)
        player.save()
        player.organisations.add(orga)
        player.save()
        form = self.app.get(reverse('swear_jar:denounce_curse',
                                    kwargs={'organisation_id': orga.id})).form
        # ask for a player name & email
        self.assertTrue('user_id' in form.fields)
        self.assertTrue('curse_text' in form.fields)

        form['player_id'] = player.user.id
        form['curse_text'] = 'putain'
        form.submit()

        curse = Curse.objects.first()
        self.assertEqual(curse.curse, 'putain')
        self.assertEqual(curse.created_at.date(), date.today())
        self.assertEqual(curse.author, player)
