from django.test import TestCase, Client
from django.test.utils import setup_test_environment
from django_webtest import WebTest

from django.core.urlresolvers import reverse

from ..models import Player, Organisation

setup_test_environment()

# Create your tests here.

class CreateOrgaTests(WebTest):
    def test_create_orga_view(self):
        form = self.app.get(reverse('swear_jar:create_organisation')).form
        # ask for organisation name
        self.assertTrue('organisation_name' in form.fields)
        # ask for a player name & email
        self.assertTrue('player_name' in form.fields)
        self.assertTrue('player_email' in form.fields)

        form['organisation_name'] = 'Dolead'
        form['player_name'] = 'Arnaud'
        form['player_email'] = 'arnaud@yopmail.com'
        form.submit()

        orga = Organisation.objects.first()
        self.assertEqual(orga.name, 'Dolead')
        player = Player.objects.first()
        self.assertEqual(player.name, 'Arnaud')
        self.assertEqual(player.email, 'arnaud@yopmail.com')
        self.assertTrue(player.admin)
