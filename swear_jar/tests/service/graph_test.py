from __future__ import division
from datetime import datetime
import unittest
from mock import Mock

import services.graph as grapher


class GraphServiceTest(unittest.TestCase):
    def test_weekly_data(self):
        player = Mock()
        player.curse_set.all.return_value = [
            Mock(created_at=datetime(2016, 9, 1)),  # week of 29^th august 2016
            Mock(created_at=datetime(2016, 9, 1)),
            Mock(created_at=datetime(2016, 9, 12)),  # week of 12^th september
        ]
        data = grapher.week_data(player)

        self.assertEqual(data, [
            {"week": "2016-08-29", "curses": 2},
            {"week": "2016-09-05", "curses": 0},
            {"week": "2016-09-12", "curses": 1},
        ])

        # Test if no data
        player.curse_set.all.return_value = []
        data = grapher.week_data(player)
        self.assertEqual(data, [])

    def test_daily_data(self):
        player = Mock()
        player.curse_set.all.return_value = [
            Mock(created_at=datetime(2016, 9, 1)),  # week of 29^th august 2016
            Mock(created_at=datetime(2016, 9, 1)),
            Mock(created_at=datetime(2016, 9, 2)),  # week of 12^th september
        ]
        data = grapher.weekday_data(player)
        nb_days = (datetime.now() - datetime(2016, 9, 1)).days
        nb_weeks = nb_days // 7 + bool(nb_days % 7)
        self.assertEqual(data, [
            {"weekday": "Monday", "curses": 0},
            {"weekday": "Tuesday", "curses": 0},
            {"weekday": "Wednesday", "curses": 0},
            {"weekday": "Thursday", "curses": round(2 / nb_weeks, 2)},
            {"weekday": "Friday", "curses": round(1 / nb_weeks, 2)},
            {"weekday": "Saturday", "curses": 0},
            {"weekday": "Sunday", "curses": 0},
        ])

        # Test if no data
        player.curse_set.all.return_value = []
        player.curse_set.count.return_value = 0
        data = grapher.weekday_data(player)
        self.assertEqual(data, [
            {"weekday": "Monday", "curses": 0},
            {"weekday": "Tuesday", "curses": 0},
            {"weekday": "Wednesday", "curses": 0},
            {"weekday": "Thursday", "curses": 0},
            {"weekday": "Friday", "curses": 0},
            {"weekday": "Saturday", "curses": 0},
            {"weekday": "Sunday", "curses": 0},
        ])

    def test_curse_count(self):
        player = Mock()
        player.curse_set.all.return_value = [
            Mock(curse="putain"),
            Mock(curse="putain"),
            Mock(curse="merde"),
        ]
        data = grapher.curse_count(player)
        self.assertEqual(data, [
            {"curse": "merde", "number": 1},
            {"curse": "putain", "number": 2},
        ])

        # Test if no data
        player.curse_set.all.return_value = []
        data = grapher.curse_count(player)
        self.assertEqual(data, [])
