from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_email
from django.core.urlresolvers import reverse
from django.http import (HttpResponse, HttpResponseRedirect, HttpResponseServerError,
                         HttpResponseForbidden, HttpResponseNotFound, JsonResponse)
from django.shortcuts import get_object_or_404, render, redirect
from django.views import generic

from .models import Curse, Organisation, Player, BankPaid, ReportedCurse
import services.graph as graph


class IndexView(generic.ListView):
    template_name = 'swear_jar/index.html'
    context_object_name = 'recent_organisations'

    def get_queryset(self):
        return Organisation.objects.order_by('-id')[:5]


class DetailView(generic.DetailView):
    model = Organisation
    template_name = 'swear_jar/detail.html'


class LoginView(generic.TemplateView):
    model = Organisation
    template_name = 'swear_jar/login.html'


def welcome(request):
    return render(request, 'swear_jar/welcome.html')


def test(request):
    return render(request, 'swear_jar/test.html')


def join_swear_jar(request):
    return render(request, 'swear_jar/joining_swear_jar.html')


def create_organisation_form(request):
    return render(request, 'swear_jar/create_organisation.html')


def created_organisation(request):
    try:
        organisation = Organisation(name=request.POST['organisation_name'])
        organisation.save()
    except Exception:
        return HttpResponseServerError("We failed while saving your data")
    if request.user.id:
        request.user.player.organisation = organisation.id
        request.user.player.admin = True
        return organisation_results(request)
    else:
        return create_player_form(request, organisation.id)
    

@login_required(login_url='/swear_jar/login/')
def denounce(request):
    organisation = request.user.player.organisation
    return render(request, 'swear_jar/denounce_curse.html', {
            'organisation': organisation,
            })

@login_required(login_url='/swear_jar/login/')
def denounced(request):
    organisation = request.user.player.organisation
    try:
        selected_player = organisation.player_set.get(pk=request.POST['player_id'])
    except (KeyError, Organisation.DoesNotExist):
        return render(request, 'swear_jar/detail.html', {
            'organisation': organisation,
            'error_message': "You did not select a player",
            })
    if selected_player == request.user.player:
        curse = Curse(curse=request.POST['curse_text'].strip(),
                      author=selected_player)
        selected_player.bank_credit -= .1
        selected_player.save()
    else:
        curse = ReportedCurse(
            curse=request.POST['curse_text'].strip(),
            author=selected_player, reporter=request.user.player,
            context=request.POST.get('curse_context', ''))
    curse.save()
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponseRedirect(reverse('swear_jar:player_home'))

@login_required(login_url='/swear_jar/login/')
def regroup_curse(request):
    for curse in request.user.player.curse_set.filter(curse=request.POST['curse_old']):
        curse.curse = request.POST['curse_new']
        curse.save()
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponseRedirect(reverse('swear_jar:player_home'))


def create_player_form(request, organisation_id=None, errors=None):
    try:
        organisation = Organisation.objects.get(pk=organisation_id)
    except Exception as DoesNotExist:
        organisation = None
    return render(request, 'swear_jar/create_player.html', {
        'organisation': organisation,
        'errors': errors})


def created_player(request, organisation_id=None):
    # Handling errors (name already taken, invalid email)
    errors = []
    if User.objects.filter(username=request.POST['username']).count():
        errors.append('username')
    try:
        validate_email(request.POST['email'])
    except ValidationError:
        errors.append('email')
    if errors:
        return create_player_form(request, organisation_id, errors)
    user = User.objects.create_user(
        username=request.POST['username'],
        email=request.POST['email'],
        password=request.POST['password'],
    )
    player = Player(user=user)
    player.name = user.username
    if organisation_id:
        player.organisation = Organisation.objects.get(pk=organisation_id)
    player.save()
    return log(request, next_page='swear_jar:player_graph')
    

def log(request, next_page='swear_jar:player_graph'):
    user = authenticate(username=request.POST['username'],
                        password=request.POST['password'])
    if not user:
        return render(request, 'swear_jar/login.html',
                      {'errors': 'Wrong username or password'})
    login(request, user)
    orga = user.player.organisation
    # Hack: you are returning on Dolead's result page
    return HttpResponseRedirect(reverse(next_page))

@login_required(login_url='/swear_jar/login/')
def logout_view(request):
    # return logout(request, next_page='/swear_jar/login')
    logout(request)
    return HttpResponseRedirect(reverse('swear_jar:login'))

 
@login_required(login_url='/swear_jar/login/')
def organisation_results(request):
    orga = request.user.player.organisation
    if not orga:
        return HttpResponseForbidden('You are not member of an organisation')
    if not player.admited:
        return render(request, 'swear_jar/results_forbidden.html')

    nb_curses = Curse.objects.filter(author__in=orga.player_set.all()).count()
    bank_orga_theo = nb_curses * .1
    bank_orga = sum(p.bank_credit for p in orga.player_set.all())
    return render(request, 'swear_jar/results.html', {
        'organisation': orga, 'bank_orga': bank_orga, 'bank_orga_theo': bank_orga_theo})


@login_required(login_url='/swear_jar/login/')
def organisation_admin(request):
    orga = request.user.player.organisation
    if not orga:
        return HttpResponseForbidden('You are not member of an organisation')
    if not request.user.player.admin:
        return render(request, 'swear_jar/not_admin.html')

    players_outside = orga.player_set.filter(admited=False)
    players_inside = orga.player_set.filter(admited=True, admin=False)
    players_admin = orga.player_set.filter(admited=True, admin=True)
    return render(request, 'swear_jar/admin_board.html', {
        'organisation': orga,
        'players_outside': players_outside,
        'players_inside': players_inside,
        'players_admin': players_admin,
    })


@login_required(login_url='/swear_jar/login/')
def change_player_status(request):
    if not request.user.player.admin:
        return render(request, 'swear_jar/not_admin.html')

    new_player = Player.objects.get(pk=request.POST['player_id'])
    if new_player.organisation != request.user.player.organisation:
        return render(request, 'swear_jar/not_admin.html')
        
    if request.POST['status'] == 'inside':
        new_player.admited = True
        new_player.user.email_user(
            subject='You joined the %s group' % new_player.organisation.name,
            message="""Hello {player_name},

{organisation} notified that you belonged here.
Click here to see your statistics, and {organisation}'s ones:

{url_logging}

See you soon on SwearJar.""".format(
                player_name=new_player.name or new_player.user.username,
                organisation=new_player.organisation.name,
                url_logging="http://swear.jarr.fr/swear_jar/player_home/"
        ))
    if request.POST['status'] == 'admin':
        new_player.admin = True
    new_player.save()
        
    return JsonResponse({
        'player': new_player.id,
        'admited': new_player.admited,
        'admin': new_player.admin})


@login_required(login_url='/swear_jar/login/')
def player_graph(request, first_time=False):
    """
    return render(request, 'swear_jar/player_graph.html')
    """
    return render(request, 'swear_jar/player_graph.html', {
        'data_weekly': graph.week_data(request.user.player),
        'data_daily': graph.weekday_data(request.user.player),
        'count_curse': graph.curse_count(request.user.player),
    })

@login_required(login_url='/swear_jar/login/')
def player_home(request, first_time=False):
    """
    return render(request, 'swear_jar/player_graph.html')
    """
    organisation = request.user.player.organisation
    curse_text = {curse.curse for curse in request.user.player.curse_set.all()}
    return render(request, 'swear_jar/player_home.html', {
        'data_weekly': graph.week_data(request.user.player),
        'data_daily': graph.weekday_data(request.user.player),
        'count_curse': graph.curse_count(request.user.player),
        'organisation': organisation,
        'curse_set': curse_text,
    })

@login_required(login_url='/swear_jar/login/')
def player_bank(request):
    return render(request, 'swear_jar/bank.html', {
            'player': request.user.player,
            })


@login_required(login_url='/swear_jar/login/')
def player_edit(request):
    return render(request, 'swear_jar/edit_player.html', {
            'player': request.user.player,
            })


@login_required(login_url='/swear_jar/login/')
def player_edited(request):
    if request.POST.get('name'):
        request.user.player.name = request.POST['name']
    if request.POST.get('email'):
        email = request.POST['email']
        try:
            validate_email(email)
            request.user.player.email = email
        except ValidationError:
            pass
    request.user.player.save()
    return render(request, 'swear_jar/edit_player.html')


@login_required(login_url='/swear_jar/login/')
def player_change_password(request):
    if not request.user.check_password(request.POST['old']):
        render(request, 'swear_jar/edit_player.html')
    if request.POST['new'] == request.POST['repeat']:
        request.user.set_password(request.POST['new'])
    return render(request, 'swear_jar/player_home.html')


@login_required(login_url='/swear_jar/login/')
def player_paid_bank(request):
    amount = request.POST['amount_paid']
    amount.replace(',', '.')
    amount = float(amount)
    bank_paid = BankPaid(amount=amount, author=request.user.player)
    bank_paid.save()
    request.user.player.bank_credit += bank_paid.amount
    request.user.player.save()
    return render(request, 'swear_jar/bank_paid.html', {
                'bank_paid': bank_paid,
            })

def search_organisation(request):
    return render(request, 'swear_jar/search_organisation.html')


ACCEPTABLE_CHARS = set(  # Should do we reg-exp ? 
    [chr(i) for i in range(65, 91)] +  # 'A-Z'
    [chr(i) for i in range(97, 123)] +  # 'a-z'
    [chr(i) for i in range(48, 58)]  # '0-9'
)

@login_required(login_url='/swear_jar/login/')
def reported_curse(request):
    player = request.user.player
    curses = player.reportedcurse_author
    return render(request, 'swear_jar/reported_curses.html',
                  {'player': player, 'curses': curses})

def api_search(request):
    query = Organisation.objects
    for word in request.POST['orga_name'].strip().split():
        word = ''.join(char for char in word if char in ACCEPTABLE_CHARS)
        query = query.filter(name__contains=word)
    response = [{'name': orga.name, 'id': orga.id} for orga in query]
    return JsonResponse(response, safe=False)


@login_required(login_url='/swear_jar/login/')
def api_join_organisation(request, organisation_id):
    request.user.player.organisation = organisation_id
    request.user.player.save()
    return HttpResponse("Organisation added")


@login_required(login_url='/swear_jar/login/')
def api_accept_curse(request):
    report = ReportedCurse.objects.get(id=request.POST['reported_curse_id'])
    curse = Curse(curse=report.curse,
                  author=report.author,
                  created_at=report.created_at)
    curse.save()
    report.author.bank_credit -= .1
    report.author.save()
    report.delete()
    return HttpResponse("{} saved for player {}".format(curse.curse, request.user.username))


@login_required(login_url='/swear_jar/login/')
def api_reject_curse(request):
    report = ReportedCurse.objects.get(id=request.POST['reported_curse_id'])
    report.delete()
    return HttpResponse("{} rejected for player {}".format(report.curse, request.user.username))
