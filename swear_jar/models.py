from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Organisation(models.Model):
    name = models.CharField(max_length=200)


class Player(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    organisation = models.ForeignKey('Organisation')
    email = models.EmailField()
    admin = models.BooleanField(default=False)
    admited = models.BooleanField(default=False)
    bank_credit = models.FloatField(default=.0)


class Curse(models.Model):
    curse = models.CharField(max_length=500)
    author = models.ForeignKey('Player')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)


class ReportedCurse(models.Model):
    curse = models.CharField(max_length=500)
    context = models.CharField(max_length=500)
    author = models.ForeignKey('Player', related_name='%(class)s_author')
    reporter = models.ForeignKey('Player', related_name='%(class)s_reporter')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    

class BankPaid(models.Model):
    amount = models.FloatField()
    author = models.ForeignKey('Player')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
