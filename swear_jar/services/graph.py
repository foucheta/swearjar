from __future__ import division
from datetime import datetime, timedelta
from collections import Counter


def week_data(player):
    """
    Returns a list of dict
        [{"week": "2016-08-29",  # Monday of the week
          "curses": 5},
        ]
    """
    week_curses = Counter()
    start_curse = datetime.now()
    for curse in player.curse_set.all():
        week_curses[curse.created_at.strftime("%Y-W%W")] += 1

    if not week_curses:
        return []
    start = datetime.strptime(min(week_curses) + "-1", "%Y-W%W-%w")
    end = datetime.strptime(max(week_curses) + "-1", "%Y-W%W-%w")
    res = []
    while start <= end:
        res.append({"week": start.strftime("%Y-%m-%d"),
                    "curses": week_curses[start.strftime("%Y-W%W")]})
        start += timedelta(7)
    return res


def weekday_data(player):
    """
    Returns a list of dict
        [{"weekday": "Monday","curses": 5}, ...]
    """
    weekday_curses = Counter()
    for curse in player.curse_set.all():
        weekday_curses[curse.created_at.strftime("%A")] += 1

    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
            'Saturday', 'Sunday']
    if player.curse_set.count():
        start = min(curse.created_at for curse in player.curse_set.all())
        # For crappy timezone stuff, I have to do this computation
        start = datetime.strptime(start.strftime("%Y-%m-%d"), "%Y-%m-%d")
        nb_days = (datetime.now() - start).days
        nb_weeks = nb_days // 7 + bool(nb_days % 7)
        nb_weeks = nb_weeks or 1
    else:
        nb_weeks = 1
    return [{"weekday": day, "curses": round(weekday_curses[day] / nb_weeks, 2)}
            for day in days]


def curse_count(player):
    count = Counter()
    for curse in player.curse_set.all():
        count[curse.curse] += 1
    return [{"curse": curse.encode("iso-8859-1"), "number": val} for curse, val in count.items()]
    
